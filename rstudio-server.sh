# login as root
sh -c 'echo "deb https://cloud.r-project.org/bin/linux/ubuntu xenial/" >> /etc/apt/sources.list'
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9
add-apt-repository -y ppa:ubuntugis/ubuntugis-unstable
apt update
apt install -y r-base r-base-dev libcurl4-openssl-dev libssl-dev libxml2-dev libgeos-dev libproj-dev libgdal-dev libudunits2-dev gdebi-core
wget https://download2.rstudio.org/rstudio-server-1.0.143-amd64.deb
gdebi rstudio-server-1.0.143-amd64.deb
adduser rstudio
